import { Sequelize } from "sequelize";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";
import { errorCode, successCode } from "../config/response.js";

const models = initModels(sequelize);

const Op = Sequelize.Op;

const createLikeRes = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;

    let newData = { user_id, res_id, date_like: Date.now() };

    let data = await models.like_res.create(newData);

    successCode(res, data, "Like thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const removeLikeRes = async (req, res) => {
  try {
    let { user_id, res_id } = req.params;

    let data = await models.like_res.destroy({ where: { user_id, res_id } });

    successCode(res, data, "Unlike thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getLikesByResAndUser = async (req, res) => {
  try {
    let data = await models.like_res.findAll({
      include: ["re", "user"],
    });

    successCode(
      res,
      data,
      "Lấy danh sách like theo nhà hàng và user thành công"
    );
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getLikesByRes = async (req, res) => {
  try {
    let { res_id } = req.params;

    let data = await models.restaurant.findAll({
      include: ["user_id_users"],
      where: { res_id },
    });

    successCode(res, data, "Lấy danh sách like theo nhà hàng thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getLikesByUser = async (req, res) => {
  try {
    let { user_id } = req.params;

    let data = await models.user.findAll({
      include: ["res_id_restaurants"],
      where: { user_id },
    });

    successCode(res, data, "Lấy danh sách like theo user thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

export {
  createLikeRes,
  removeLikeRes,
  getLikesByResAndUser,
  getLikesByRes,
  getLikesByUser,
};
