import { Sequelize } from "sequelize";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";
import { errorCode, successCode } from "../config/response.js";

const models = initModels(sequelize);

const Op = Sequelize.Op;

const createRateRes = async (req, res) => {
  try {
    let { user_id, res_id, amount } = req.body;

    let newData = { user_id, res_id, amount, date_rate: Date.now() };

    let data = await models.rate_res.create(newData);

    successCode(res, data, "Rate thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getRatesByResAndUser = async (req, res) => {
  try {
    let data = await models.rate_res.findAll({
      include: ["re", "user"],
    });

    successCode(
      res,
      data,
      "Lấy danh sách đánh giá theo nhà hàng và user thành công"
    );
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getRatesByRes = async (req, res) => {
  try {
    let { res_id } = req.params;

    let data = await models.restaurant.findAll({
      include: ["user_id_user_rate_res"],
      where: { res_id },
    });

    successCode(res, data, "Lấy danh sách đánh giá theo nhà hàng thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getRatesByUser = async (req, res) => {
  try {
    let { user_id } = req.params;

    let data = await models.user.findAll({
      include: ["res_id_restaurant_rate_res"],
      where: { user_id },
    });

    successCode(res, data, "Lấy danh sách đánh giá theo user thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

export { createRateRes, getRatesByResAndUser, getRatesByRes, getRatesByUser };
