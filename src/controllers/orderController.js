import { Sequelize } from "sequelize";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";
import { errorCode, successCode } from "../config/response.js";

const models = initModels(sequelize);

const Op = Sequelize.Op;

const createOrder = async (req, res) => {
  try {
    let { user_id, food_id, amount, code, arr_sub_id } = req.body;

    let checkOrder = await models.order.findAll({
      where: { user_id, food_id },
    });

    let data;
    if (checkOrder.length > 0) {
      data = await models.order.update(
        {
          user_id,
          food_id,
          amount: checkOrder[0].amount + amount,
          code,
          arr_sub_id,
        },
        { where: { user_id, food_id } }
      );
    } else {
      let newData = { user_id, food_id, amount, code, arr_sub_id };

      data = await models.order.create(newData);
    }

    successCode(res, data, "Order thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

export { createOrder };
