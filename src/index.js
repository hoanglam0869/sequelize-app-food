import express from "express";
import cors from "cors";
import rootRouter from "./routers/rootRouter.js";

const app = express();

app.use(express.json());

app.use(
  cors({
    origin: ["http://localhost:5500", "http://127.0.0.1:5500"],
  })
);

app.listen(8080);

app.use("/api", rootRouter);
