import express from "express";
import {
  createRateRes,
  getRatesByRes,
  getRatesByResAndUser,
  getRatesByUser,
} from "../controllers/rateResController.js";

const rateResRouter = express.Router();

rateResRouter.post("", createRateRes);
rateResRouter.get("/get-rates-by-res-and-user", getRatesByResAndUser);
rateResRouter.get("/res/:res_id", getRatesByRes);
rateResRouter.get("/user/:user_id", getRatesByUser);

export default rateResRouter;
