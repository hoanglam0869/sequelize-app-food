import express from "express";
import {
  createLikeRes,
  getLikesByRes,
  getLikesByResAndUser,
  getLikesByUser,
  removeLikeRes,
} from "../controllers/likeResController.js";

const likeResRouter = express.Router();

likeResRouter.post("", createLikeRes);
likeResRouter.delete("/:user_id/:res_id", removeLikeRes);
likeResRouter.get("/get-likes-by-res-and-user", getLikesByResAndUser);
likeResRouter.get("/res/:res_id", getLikesByRes);
likeResRouter.get("/user/:user_id", getLikesByUser);

export default likeResRouter;
