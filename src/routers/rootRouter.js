import express from "express";
import likeResRouter from "./likeResRouter.js";
import rateResRouter from "./rateResRouter.js";
import orderRouter from "./orderRouter.js";

const rootRouter = express.Router();

rootRouter.use("/like", likeResRouter);
rootRouter.use("/rate", rateResRouter);
rootRouter.use("/order", orderRouter);

export default rootRouter;
